This module, Modal Webform, allows you to require people to fill out a form

before viewing specific content types on your site. A person need only fill

out the form one time, then all other content protected by the module is open

to be viewed by the person.



As an example, you want to generate leads for people viewing white papers

on your site. Each white paper is another node of content type whitepaper.

With this module you can easily cause a form (from the webform module) to

popup (via the lightbox2 module) over a whitepaper node. The person will

need to fill out the form before continueing to view the content. If they

then view another white paper on your site, the form will not popup again. 



Module settings allow you to specify the webform that will display in the

popup, as well as which content types should be "protected" by the popup form.



Modal Webform requires webform v3 (http://drupal.org/project/webform) and 

lightbox2 (http://drupal.org/project/lightbox2).